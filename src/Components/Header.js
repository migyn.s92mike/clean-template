import React, { Component } from 'react';

export class Header extends Component {
  static defaultProps = {
    menus: [
      { title: "Home", link: "/" }
    ]
  }
  render() {
    const { menus } = this.props;
    return (
      <header className="App-header">
        <ul>
          {menus.map(({ title, link }, i)=><li key={i}><a href={link}>{title}</a></li>)}
        </ul>
      </header>
    );
  }
}

export default Header;
