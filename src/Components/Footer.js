import React from 'react';
import { __FOOTER_TEXT as copyrights } from '../Utils/Constants';


export const Footer = () => {
  return (<footer dangerouslySetInnerHTML={{ __html: copyrights }} />);
}

export default Footer;
